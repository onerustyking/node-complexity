exports = class PolarComplex
  constructor: (@_mag,@_arg) ->

  mag: ->
    @_mag

  arg: ->
    @_arg

  real: ->
    @_mag * Math.cos(@_arg)

  imag: ->
    @_mag * Math.sin(@_arg)

  add: (other) ->
    @toRectangularComplex().add(other.toRectangularComplex())

  sub: (other) ->
    @toRectangularComplex().sub(other.toRectangularComplex())

  multiply: (other) ->
    other = other.toPolarComplex()
    mag = @_mag * other.mag()
    arg = @_arg + other.arg()
    new PolarComplex(mag,arg)

  divide: (other) ->
    other = other.toPolarComplex()
    mag = @_mag / other.mag()
    arg = @_arg - other.arg()
    new PolarComplex(mag,arg)

  toRectangularComplex: ->
    new RectangularComplex(@real(),@imag())

  toPolarComplex: ->
    new PolarComplex(@_mag,@_arg)

