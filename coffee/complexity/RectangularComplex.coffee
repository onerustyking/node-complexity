exports = class RectangularComplex
  constructor: (@_real,@_imag) ->

  real: ->
    @_real

  imag: ->
    @_imag

  mag: ->
    Math.sqrt(Math.pow(@_real,2) + Math.pow(@_imag,2))

  arg: -> 
    Math.atan2(@_imag,@_real)

  add: (other) ->
    other = other.toRectangularComplex()
    real = @_real + other.real()
    imag = @_imag + other.imag()
    new RectangularComplex(real,imag)

  sub: (other) ->
    other = other.toRectangularComplex()
    real = @_real - other.real()
    imag = @_imag - other.imag()
    new RectangularComplex(real,imag)

  multiply: (other) ->
    @toPolarComplex().multiply(other.toPolarComplex())

  divide: (other) ->
    @toPolarComplex().divide(other.toPolarComplex())

  toPolarComplex: ->
    new PolarComplex(@mag(),@arg())

  toRectangularComplex: ->
    new RectangularComplex(@_real,@_imag)

