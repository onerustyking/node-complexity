(function() {
  var root, _ref;

  root = (_ref = typeof exports !== "undefined" && exports !== null) != null ? _ref : {
    window: exports
  };

  root.Complexity = {};

  root.Complexity.PolarComplex = require('./complexity/PolarComplex.js');

  root.Complexity.RectangularComplex = require('./complexity/RectangularComplex.js');

}).call(this);
