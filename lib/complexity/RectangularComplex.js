(function() {
  var RectangularComplex, exports;

  exports = RectangularComplex = (function() {

    function RectangularComplex(_real, _imag) {
      this._real = _real;
      this._imag = _imag;
    }

    RectangularComplex.prototype.real = function() {
      return this._real;
    };

    RectangularComplex.prototype.imag = function() {
      return this._imag;
    };

    RectangularComplex.prototype.mag = function() {
      return Math.sqrt(Math.pow(this._real, 2) + Math.pow(this._imag, 2));
    };

    RectangularComplex.prototype.arg = function() {
      return Math.atan2(this._imag, this._real);
    };

    RectangularComplex.prototype.add = function(other) {
      var imag, real;
      other = other.toRectangularComplex();
      real = this._real + other.real();
      imag = this._imag + other.imag();
      return new RectangularComplex(real, imag);
    };

    RectangularComplex.prototype.sub = function(other) {
      var imag, real;
      other = other.toRectangularComplex();
      real = this._real - other.real();
      imag = this._imag - other.imag();
      return new RectangularComplex(real, imag);
    };

    RectangularComplex.prototype.multiply = function(other) {
      return this.toPolarComplex().multiply(other.toPolarComplex());
    };

    RectangularComplex.prototype.divide = function(other) {
      return this.toPolarComplex().divide(other.toPolarComplex());
    };

    RectangularComplex.prototype.toPolarComplex = function() {
      return new PolarComplex(this.mag(), this.arg());
    };

    RectangularComplex.prototype.toRectangularComplex = function() {
      return new RectangularComplex(this._real, this._imag);
    };

    return RectangularComplex;

  })();

}).call(this);
