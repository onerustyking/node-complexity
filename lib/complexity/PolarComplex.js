(function() {
  var PolarComplex, exports;

  exports = PolarComplex = (function() {

    function PolarComplex(_mag, _arg) {
      this._mag = _mag;
      this._arg = _arg;
    }

    PolarComplex.prototype.mag = function() {
      return this._mag;
    };

    PolarComplex.prototype.arg = function() {
      return this._arg;
    };

    PolarComplex.prototype.real = function() {
      return this._mag * Math.cos(this._arg);
    };

    PolarComplex.prototype.imag = function() {
      return this._mag * Math.sin(this._arg);
    };

    PolarComplex.prototype.add = function(other) {
      return this.toRectangularComplex().add(other.toRectangularComplex());
    };

    PolarComplex.prototype.sub = function(other) {
      return this.toRectangularComplex().sub(other.toRectangularComplex());
    };

    PolarComplex.prototype.multiply = function(other) {
      var arg, mag;
      other = other.toPolarComplex();
      mag = this._mag * other.mag();
      arg = this._arg + other.arg();
      return new PolarComplex(mag, arg);
    };

    PolarComplex.prototype.divide = function(other) {
      var arg, mag;
      other = other.toPolarComplex();
      mag = this._mag / other.mag();
      arg = this._arg - other.arg();
      return new PolarComplex(mag, arg);
    };

    PolarComplex.prototype.toRectangularComplex = function() {
      return new RectangularComplex(this.real(), this.imag());
    };

    PolarComplex.prototype.toPolarComplex = function() {
      return new PolarComplex(this._mag, this._arg);
    };

    return PolarComplex;

  })();

}).call(this);
